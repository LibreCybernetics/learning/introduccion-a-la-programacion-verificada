% Introducción a la Programación Verificada
% Fabián Heredia Montiel

# Prefacio

(Algo de info previa)

## Fabián Heredia

 - Estudiante de Matemáticas
 - @Cyb3rPyscho
 
## Objetivos

 - Dar una sobrevista de métodos formales
 - Sentar las bases de verificación de código

# Métodos Formales

## Diferentes Áreas

 - Especificación de Diseño
 - Verificación de Diseño

. . .

 - Especificación de Código
 - Verificación de Código
 
## Especificación y Verificación de Diseño

No es parte del objetivo del curso

Álgunos Ejemplos: TLA+, Alloy, Rebecca

## Especificación de Código

 - Cubre el Dominio
 - Totalidad
 - Productividad
 
. . .

 - Expresar invariantes / propiedades que se cumplen.
 
## Verificación de Código

 - Dada una especificación de código, determinar si un código la cumple de forma estática.

 - Dos acercamientos:
   - Tipos de Refinamiento (LiquidHaskell, refined (Scala), etc)
   - Tipos Dependientes (Coq, Agda, Idris, etc)

## Ejemplos

Haskell tiene errores de runtime (¿Me mintieron que los tipos me salvaban? D:)

. . .

    > head []
    *** Exception: Prelude.head: empty list

. . .

    > let t = pack "Hola Mundo! C:" -- Data.Text
    > takeWord16 20 t
    "Hola Mundo! C:\32741\NUL\40689{B\NUL" -- Fuga de Memoria :S
    
## ATM

<video autoplay loop>
  <source src="img/atm.mp4" type="video/mp4"/>
</video>

## Otros

¿Qué ejemplos de errores conocen que hayan costado muchos recursos o causado heridas o muerte?

# Bases Teóricas

## En Este modulo

 - Algo de Lógica
 - Historia Breve Imprecisa de Computabilidad
 - Calculo Lambda
 - Tésis Church-Turing
 - Calculo Lambda Simplemente Tipado
 - Constructores de Tipos, Polimorfismo, Tipos Dependientes
 - Correspondencia Curry-Howard

## Lógica Intuisionista

 - No cuenta con la eliminación de doble negación
 - Equivalentemente no hay tercer excluido ($P \vee \neg P$)
 - Probar que "No estuvo mal" no implica que "estuvo bien"; puede ser neutral.

## Historia Breve de la Computabilidad

 - Programa de Hilbert
 - Funciones Recursivas Generales (Herbrand-Gödel 1933)
 - Cálculo Lambda (Alonzo Church 1936)
 - Máquina de Turing (Alan Turing 1936)

## Tésis Church-Turing

Algo es computable si y solo si:

 - Existe una maquina de turing; o
 - Se puede escribir con calculo lambda; o
 - Se puede escribir como una función recursiva general.

(No se ha probado porque depende de definir "qué es computable" pero se aceptan como equivalentes)

## Cálculo Lambda

Terminos:

 - Terminos (t): v | l | a
 - Variables (v): `x,y,z,...`
 - Abstracciones/Lambdas (l): `λx.t`
 - Aplicaciones (a): `t t`
 
(Para reducir, hay β-reducción que es reemplazar)

## Problemas con Cálculo Lambda

Es demasiado poderoso:

    ω = λx.(x x)
    Ω = ω ω

. . .

Primera reducción:

    Ω = ω ω
    
. . .

Etc...

## Tipos

Consideramos $\Gamma$ un contexto con elementos y su notación de tipos.

 - Si $x : \tau \in \Gamma$ entonces $x$ tiene tipo $\tau$ en $\Gamma$.
 - Si $t: \tau, x: \sigma \in \Gamma$ entonces $\lambda x.t$ tiene el tipo $\sigma \rightarrow \tau$ en $\Gamma$
 - (Si se aplica tiene tipo $\tau$, etc)


## Cubo Lambda

Los tipos son demasiado sencillos, restringen demasiado

Podemos añadir:

 - Constructores (`data Bool = True | False`)
 - Polimorfismo  (`data Maybe a = Nothing | Just a`)
 - Tipos Dependientes (`take :: (l: List a) -> (n: Int) -> {0 <= n && n < len l} -> List a`)

## Correspondencia Curry-Howard

Tambien conocido como:

 - _tipos-como-proposiciones_
 - _programas-como-pruebas_

# Ejercicios

## Nat

    data Natural = Cero | S Natural
    
Definan:

 - suma y producto
 
. . .

 - resta y division
 
. . .

 - Hagan alguna prueba

## Maquína de Estados (ATM)

Estados: EnEspera | ChecarPIN | Menu

Transiciones: insertarTarjeta: EnEspera -> ChecarPIN
              checarPin: ChecarPIN -> {EnEspera | Menu}
              checarEstado: Menu -> Menu
              retirar: Menu -> EnEspera (Sacar Tarjeta y Dinero)
              
